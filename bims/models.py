from django.db import models
from options import Options

options = Options()

class Investigator(models.Model):
    """Captures information about contact for project"""
    investigator_name = models.CharField(max_length=200)
    department = models.IntegerField(choices=options.DEPARTMENT_CHOICES)
    investigator_email = models.EmailField()
    def __unicode__(self):
        return self.investigator_name

class BioinformaticsSupport(models.Model):
    """Information about members of the bioinformatics support unit"""
    support_name = models.CharField(max_length=200)
    support_email = models.EmailField()
    hourly_rate = models.DecimalField(decimal_places=2, max_digits=6)
    def __unicode__(self):
        return self.support_name
class ExperimentType(models.Model):
    """Different classes of experiment"""
    type_name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.type_name

class Project(models.Model):
    """Captures core information about project"""
    project_name = models.CharField(max_length=200)
    project_description = models.TextField()
    project_lead = models.ForeignKey(Investigator)
    bsu_lead = models.ForeignKey(BioinformaticsSupport)
    project_start_date = models.DateField()
    project_end_date = models.DateField(null=True)
    project_complete = models.BooleanField()
    hours_worked = models.DecimalField(decimal_places=1, max_digits=6, null=True)
    def __unicode__(self):
        return self.project_name

class Experiment(models.Model):
    """An individual experiment, sits within a Project,
            and has an ExperimentType"""
    experiment_name = models.CharField(max_length=200)
    experiment_type = models.ForeignKey(ExperimentType)
    in_project = models.ForeignKey(Project)
    def __unicode__(self):
        return self.experiment_name

class ProjectNotes(models.Model):
    pass

class ProjectToDos(models.Model):
    pass
